## Ejercicio - String más largo

Crea el método `longest` que recibe como parámetro un `Array` de `Strings` y regresa el string(s) más largo dentro de un arreglo.

Se entregarán dos versiones de este programa, la primera usando `estructura iterativa` y la otra versión usando solamente `métodos enumerables`.



```ruby
#longest method


#Driver code

p longest(['tres', 'pez', 'alerta', 'cuatro', 'tesla', 'tropas', 'siete']) == ["alerta", "cuatro", "tropas"]
p longest(['gato', 'perro', 'elefante', 'jirafa']) == ["elefante"]
p longest(['verde', 'rojo', 'negro', 'morado']) == ["morado"]
```